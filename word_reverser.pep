; WORD REVERSER
;
; author: Philippe Nadon
; course: AUCSC 250
;   date: October 15, 2018
; system: Pep/9 simulator
; 
; A program which takes alphabetical characters
; and prints them in reverse order


; Reserved memory for data
BR introMsg, i 
chars: .BLOCK 40
message: .ASCII "WORD REVERSER\nEnter a word: \x00"
error: .ASCII "You did not enter any word. All words must be made with letters A to Z and a to z only\x00"


; can only access this spot via branch
; Used to print error message before exiting
errorMsg: STRO error, d
BR endPrgrm, i




; |-------------------|
; |PROGRAM STARTS HERE|
; |-------------------|
introMsg:  STRO message, d
LDWX -1, i 


; inChars repeatedly takes an input until
; a non-alphabetical character is inputted
;
; index register keeps track of the index
; of characters inputted
inChars: LDBA 0xFC15, d
ADDX 1, i
STBA chars, x 

; Branches to outChars if 
; maximum characters reached
CPWX 40, i
BRGE outChars, i

; Checks if outside of range of A-z,
; exits inChars if true
LDBA chars, x
CPWA 65, i
BRLT check1st, i
CPWA 122, i
BRGT check1st, i

; Checks if A-Z, or a-z,
; Branches back to inChars if true
CPWA 90, i
BRLE inChars, i
CPWA 97, i
BRGE inChars, i



; If check1st is reached on the
; first character, then branch to
; errorMsg to print error and exit
check1st: CPWX 0, i
BRLE errorMsg, i

; Starting from current index, print
; character then go down by one until
; you reach the first character inputted
outChars: ADDX -1, i
LDBA chars, x 
STBA 0xFC16, d
CPWX 0, i
BRGT outChars, i
endPrgrm: STOP 
.END